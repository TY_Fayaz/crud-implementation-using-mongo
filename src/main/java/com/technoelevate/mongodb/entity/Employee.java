package com.technoelevate.mongodb.entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Document(collection ="employee" )
public class Employee {
	@Transient //id autogeneration
	public static final String SEQUENCE_NAME="employee_sequence";
	@Id
	private int employeeId;

	private String name;
	private long phoneNo;
	private String emailId;

}
