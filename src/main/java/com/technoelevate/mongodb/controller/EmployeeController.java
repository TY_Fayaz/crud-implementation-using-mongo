package com.technoelevate.mongodb.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.technoelevate.mongodb.entity.Employee;
import com.technoelevate.mongodb.service.EmployeeService;
import com.technoelevate.mongodb.service.SequenceGeneratorService;

@RestController
public class EmployeeController {
	
	@Autowired
	private EmployeeService service;
	
	@Autowired
	private SequenceGeneratorService generatorService;
	
	@PostMapping("/add")
	public String saveEmployee(@RequestBody Employee employee) {
		employee.setEmployeeId(generatorService.getSequenceNumber(Employee.SEQUENCE_NAME));
		return service.saveEmployee(employee);
	}
	
	@GetMapping("/getAll")
	public List<Employee> getEmployees(){
		return service.getEmployees();
	}
	
	@GetMapping("/getOne/{employeeId}")
	public Optional<Employee> getEmployee(@PathVariable int employeeId) {
		return service.getEmployee(employeeId);
	}
	
	@DeleteMapping("/delete/{employeeId}")
	public String deleteEmployee(@PathVariable int employeeId) {
		return service.deleteEmployee(employeeId);
	}
	
	

}
