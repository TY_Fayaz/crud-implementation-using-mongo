package com.technoelevate.mongodb.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.technoelevate.mongodb.entity.Employee;
import com.technoelevate.mongodb.repository.EmployeeRepository;
@Service
public class EmployeeServiceImpl implements EmployeeService {

	@Autowired
	private EmployeeRepository repository;
	
	@Override
	public  String saveEmployee(Employee employee) {
		
		repository.save(employee);
		return "employee added with id "+employee.getEmployeeId();
		
	}

	@Override
	public List<Employee> getEmployees() {
	   return	repository.findAll();
		
	}

	@Override
	public Optional<Employee> getEmployee(int employeeId) {

		return repository.findById(employeeId);
	}

	@Override
	public String deleteEmployee(int employeeId) {
    
		repository.deleteById(employeeId);
	    return "employee deleted successfully";	
	}

}
