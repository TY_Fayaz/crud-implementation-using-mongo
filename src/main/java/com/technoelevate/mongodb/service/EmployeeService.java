package com.technoelevate.mongodb.service;

import java.util.List;
import java.util.Optional;

import com.technoelevate.mongodb.entity.Employee;

public interface EmployeeService {

	public String saveEmployee(Employee employee);

	public List<Employee> getEmployees();

	public Optional<Employee> getEmployee(int employeeId);

	public String deleteEmployee(int employeeId);

}
