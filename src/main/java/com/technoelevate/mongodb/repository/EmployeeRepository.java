package com.technoelevate.mongodb.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.technoelevate.mongodb.entity.Employee;

public interface EmployeeRepository extends MongoRepository<Employee, Integer>{

}
